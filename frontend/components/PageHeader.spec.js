import { RouterLinkStub, createLocalVue, mount } from '@vue/test-utils';
import test from 'ava';

import Vue from 'vue'
import Vuex from 'vuex';

import NuxtLink from '../.nuxt/components/nuxt-link'
import PageHeader from './PageHeader.vue'

Vue.config.silent = true

const getWrapper = (loggedIn) => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  localVue.component('nuxt-link', NuxtLink)

  const store = new Vuex.Store({
    getters: { isAuthenticated: () => loggedIn }
  })

  const wrapper = mount(PageHeader, {
    localVue,
    store,
    stubs: {
      RouterLink: RouterLinkStub
    }
  })

  return wrapper
}

test('PageHeader should show /rooms link when user authenticated', t => {
  const wrapper = getWrapper(true)

  const item = wrapper.find('.navbar-menu .navbar-item a')

  t.is(item.text(), 'Rooms')
  t.is(item.props().to, '/rooms')
})

test('PageHeader should not show /rooms link when user is not authenticated', t => {
  const wrapper = getWrapper(false)

  t.is(wrapper.find(".navbar-menu .navbar-item a").exists(), false)
})
