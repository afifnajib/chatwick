import { createLocalVue, mount } from '@vue/test-utils';
import test from 'ava';

import Buefy from 'buefy'
import Vue from 'vue'

import SignupForm from './SignupForm.vue'

Vue.config.silent = true

test.beforeEach(t => {
  const localVue = createLocalVue()
  localVue.component('b-input', Buefy.Input)
  localVue.component('b-field', Buefy.Field)
  const wrapper = mount(SignupForm, {
    localVue
  })
  wrapper.setData({
    email: {
      value: '',
      fieldType: '',
      errorMessages: []
    },
    password: {
      value: '',
      fieldType: '',
      errorMessages: []
    }
  })
  t.context.wrapper = wrapper
  t.context.localVue = localVue
})

test('SignupForm shows error message when inputting an invalid email address without the @ sign', async t => {
  const { wrapper, localVue } = t.context
  wrapper.find('input[type="email"]').setValue('afifnajib')

  await localVue.nextTick()

  const messageLabel = wrapper.find('p.help')
  t.is(messageLabel.text(), 'Not a valid email address')
  t.is(messageLabel.attributes().class, 'help is-danger')
})

test('SignupForm shows error message when inputting an invalid email address with @ at the start', async t => {
  const { wrapper, localVue } = t.context
  wrapper.find('input[type="email"]').setValue('@gmail.com')

  await localVue.nextTick()

  const messageLabel = wrapper.find('p.help')
  t.is(messageLabel.text(), 'Not a valid email address')
  t.is(messageLabel.attributes().class, 'help is-danger')
})

test('SignupForm shows error message when inputting a valid email address', async t => {
  const { wrapper, localVue } = t.context
  wrapper.find('input[type="email"]').setValue('afifnajib@gmail.com')

  await localVue.nextTick()

  const messageLabel = wrapper.find('p.help')
  t.is(messageLabel.text(), '')
  t.is(messageLabel.attributes().class, 'help is-success')
})
