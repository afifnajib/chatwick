// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
const FRONTEND_URL = Cypress.env('FRONTEND_URL')

Cypress.Commands.add('login', (email, password) => {
  cy.visit(`${FRONTEND_URL}/login`)
  cy.wait(500)
  cy.get('input.input[type=email]').type(' testuser@gmail.com')
  cy.get('input.input[type=password]').type('12345678')
  cy.get('.button.is-primary').click()
  cy.wait(500)
  cy.matchUrl('$')
  cy.getCookie('chatwick_token').should('have.property', 'value')
})

Cypress.Commands.add('matchUrl', (urlPrefix) => {
  const urlRegex = new RegExp(FRONTEND_URL + '/' + urlPrefix)

  cy.url().should('match', urlRegex)
})
