const FRONTEND_URL = Cypress.env('FRONTEND_URL')

describe('Log in', function () {
  context('With valid credentials', function() {
    it(`Redirects to / when there's no ?next= parameter`, function() {
      cy.visit(`${FRONTEND_URL}/login`)

      cy.wait(1000)

      cy.get('input.input[type=email]').type(' testuser@gmail.com')
      cy.get('input.input[type=password]').type('12345678')

      cy.get('.button.is-primary').click()

      cy.matchUrl('$')
    })
  })

  context('With invalid credentials', function() {
    it(`displays an error message when giving a wrong password`, function() {
      cy.visit(`${FRONTEND_URL}/login`)

      cy.wait(1000)

      cy.get('input.input[type=email]').type(' testuser@gmail.com')
      cy.get('input.input[type=password]').type('wrongpassword')

      cy.get('.button.is-primary').click()

      cy.get('.notification.is-danger').should('have.length', 1)

      cy.contains('Please double-check your email and password')
    })

    it(`displays an error message when giving a wrong username`, function() {
      cy.visit(`${FRONTEND_URL}/login`)

      cy.wait(1000)

      cy.get('input.input[type=email]').type('afifnajib+nonexistent@gmail.com')
      cy.get('input.input[type=password]').type('12345678')

      cy.get('.button.is-primary').click()

      cy.get('.notification.is-danger').should('have.length', 1)

      cy.contains('Please double-check your email and password')
    })
  })
})
