const chance = require('chance').Chance()

const FRONTEND_URL = Cypress.env('FRONTEND_URL')

describe('Sign up', function () {
  context('With valid inputs', function() {
    it(`Redirects to the home page and set the right cookie`, function() {
      cy.visit(`${FRONTEND_URL}/signup`)

      cy.get('input.input[type=email]').type(chance.email())
      cy.get('input.input[type=text]').type(chance.name())
      cy.get('input.input[type=password]').type('12345678')

      cy.get('.button.is-primary').click()

      cy.matchUrl('$')
    })
  })

  context('With invalid credentials', function() {
    it(`displays an error message when an invalid email is given`, function() {
      cy.visit(`${FRONTEND_URL}/signup`)

      cy.get('input.input[type=email]').type('somewrongemailformat')
      cy.get('input.input[type=text]').type(chance.name())
      cy.get('input.input[type=password]').type('12345678')

      cy.get('.button.is-primary').click()

      cy.get('.notification.is-danger').should('have.length', 1)

      cy.contains('Your account failed to create. Please double-check your input.')
    })
  })
})
