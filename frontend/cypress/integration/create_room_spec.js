const FRONTEND_URL = Cypress.env('FRONTEND_URL')

const chance = require('chance').Chance()

describe('Create room', function () {
  it('Redirects to the created room', function() {
    cy.login('testuser@gmail.com', '12345678')

    const chatroomName = chance.sentence({ words: 3 })

    // Verify that there's no preexisting chatroom
    cy.visit(`${FRONTEND_URL}/rooms`)
    cy.get('.panel-block.room').should('not.contain', chatroomName)

    // Create room and verify redirected
    cy.get('a.card-footer-item').click()
    cy.get('input.input[type=text]').type(chatroomName)
    cy.get('a.card-footer-item').click()
    cy.wait(500)

    cy.get('.card-header-title').contains(chatroomName)
    cy.matchUrl('rooms/\\d+$')
  })

  it('Lists the room in /rooms page', function() {
    cy.login('testuser@gmail.com', '12345678')

    const chatroomName = chance.sentence({ words: 3 })

    // Create room
    cy.visit(`${FRONTEND_URL}/rooms/new`)
    cy.get('input.input[type=text]').type(chatroomName)
    cy.wait(500)
    cy.get('a.card-footer-item').click()
    cy.wait(500)

    // Go back to /rooms page and verify that the room is listed
    cy.visit(`${FRONTEND_URL}/rooms`)
    cy.get('.panel-block.room').should('contain', chatroomName)
  })

  it(`Displays error message when room name has been taken, and removes the
      error message when the user inputs something`, function () {
    cy.login('testuser@gmail.com', '12345678')

    const chatroomName = chance.sentence({ words: 3 })
    // Create the room
    cy.visit(`${FRONTEND_URL}/rooms/new`)
    cy.get('input.input[type=text]').type(chatroomName)
    cy.wait(500)
    cy.get('a.card-footer-item').click()
    cy.wait(500)

    // Create room with the same name
    cy.visit(`${FRONTEND_URL}/rooms/new`)
    cy.wait(500)
    cy.get('input.input[type=text]').type(chatroomName)
    cy.get('a.card-footer-item').click()
    cy.wait(500)

        cy.get('.notification.is-danger').should(
      'contain',
      'The room name has been taken.'
    )

    cy.get('input.input[type=text]').clear().type(chance.sentence({ words: 3 }))

    cy.get('.notification.is-danger').should('have.length', 0)
  })
})
