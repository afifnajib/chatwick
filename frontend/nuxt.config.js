module.exports = {
  head: {
    title: 'Chatwick | Make Chat Wicked Again',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Racing+Sans+One' }
    ]
  },
  env: {
    BACKEND_API_URL: process.env.BACKEND_API_URL || 'http://localhost:4007',
    AUTH_STORAGE_KEY: 'chatwick_token'
  },
  loading: { color: '#3B8070' },
  build: {
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    vendor: [
      '~/lib/feathers/service.js',
    ]
  },
  modules: [
    'nuxt-buefy'
  ]
}
