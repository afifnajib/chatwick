import Vue from 'vue'

export const state = () => ({
  newMessage: '',
  messages: [],
  fetching: false
})

export const mutations = {
  'clear-messages' (state) {
    Vue.set(state, 'messages', [])
  },
  'add-message' (state, message) {
    state.messages.push(message)
  },
  'composer-update' (state, newMessage) {
    Vue.set(state, 'newMessage', newMessage)
  },
  'fetching' (state, newState) {
    Vue.set(state, 'fetching', newState)
  },
  'clear-messages' (state, newState) {
    Vue.set(state, 'messages', [])
  }
}
