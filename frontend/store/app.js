import Cookies from 'cookie'

import Vue from 'vue'

import { SET_LOGIN, SET_USER_ID } from '../lib/mutations';
import { getCookieString } from '../lib/utils/get-cookie';
import app, { socket } from '../lib/feathers/service';

export const state = () => ({
  loggedIn: {
    client: false,
    server: false
  },
  userId: undefined
})

export const mutations = {
  [SET_LOGIN] (state, newState) {
    Vue.set(state.loggedIn, process.server ? 'server' : 'client', newState)
  },
  [SET_USER_ID] (state, userId) {
    Vue.set(state, 'userId', userId)
  }
}

export const actions = {
  async login ({ commit, state }, req) {
    if (process.server && state.loggedIn.server) { return }
    if (process.client && state.loggedIn.client) { return }

    try {
      const cookieString = getCookieString(process.server, req)
      const cookies = Cookies.parse(cookieString)
      const accessToken = cookies[process.env.AUTH_STORAGE_KEY]

      const payload = await socket.emitAsync('authenticate', {
        strategy: 'jwt',
        accessToken
      })

      const { userId } = await app.passport.verifyJWT(payload.accessToken)

      commit(SET_LOGIN, true)
      commit(SET_USER_ID, userId)

    } catch (err) {
      console.error('Error logging in', err);
      this.$router.go('/login')
    }
  },

  async findRooms () {
    const { data: rooms } = await app.service('rooms').find()

    return rooms
  }
}

export const getters = {
  isAuthenticated (state) {
    return state.loggedIn.server || state.loggedIn.client
  }
}
