import Cookies from 'cookie'

import app from '~/lib/feathers/service'
import { getCookieString } from '~/lib/utils/get-cookie';

export default async function ({ redirect, req, route, store }) {
  if (!store.getters.isAuthenticated) {
    try {
      await store.dispatch('app/login', req)
    } catch (err) {
      console.error('err', err);
      return redirect(`/login?next=${encodeURIComponent(route.path)}`)
    }
  }
}
