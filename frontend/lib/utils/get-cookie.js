export const getCookieString = (isServer, req) => {
  if (isServer) {
    return req.headers.cookie || ''
  }

  return document.cookie || ''
}
