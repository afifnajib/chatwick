import feathers from '@feathersjs/feathers'
import socketio from '@feathersjs/socketio-client'
import io from 'socket.io-client'
import auth from '@feathersjs/authentication-client'
import moment from 'moment'
import { CookieStorage } from 'cookie-storage'
import { promisify } from 'bluebird'

const socket = io(process.env.BACKEND_API_URL)
socket.emitAsync = promisify(socket.emit)

const app = feathers();

app.configure(socketio(socket))

const authentication = auth({
  header: 'Authorization',
  path: '/authentication',
  jwtStrategy: 'jwt',
  entity: 'user',
  service: 'users',
  cookie: process.env.AUTH_STORAGE_KEY,
  storageKey: process.env.AUTH_STORAGE_KEY,
  storage: new CookieStorage({
    path: '/',
    expires: moment().add(1, 'days').toDate()
  })
})

app.configure(authentication)

export { socket };

export default app
