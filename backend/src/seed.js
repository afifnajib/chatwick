const TEST_EMAIL = 'testuser@gmail.com';

export default async function seed(app) {
  // So that we can set this in config/<env>.json
  if (!app.get('seed')) {
    return;
  }

  try {
    const {total: count} = await app.service('users').find({
      query: {
        email: TEST_EMAIL,
        $limit: 0
      }
    });

    if (count > 0) {
      return;
    }

    await app.service('users').create({
      email: TEST_EMAIL,
      username: 'Afif Sohaili',
      password: '12345678'
    });
  } catch (err) {
    console.error('Error seeding data:\n', err);
  }
}
