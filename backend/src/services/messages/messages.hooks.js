const {authenticate} = require('@feathersjs/authentication').hooks;
const {fastJoin} = require('feathers-hooks-common');

const messageResolvers = {
  joins: {
    creator: users => async message => {
      const creator = (await users.get(message.createdBy));
      const {username} = creator;

      message.creator = {username};
    }
  }
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [
      fastJoin(messageResolvers, ({app}) => {
        return ({creator: {args: [app.service('users')]}});
      })
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
