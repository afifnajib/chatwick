const {authenticate} = require('@feathersjs/authentication').hooks;

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      async context => {
        const {app, result: participation, params: {user}} = context;
        const {connections} = app.channel(app.channels)
          .filter(connection => connection.user && connection.user.id === user.id);

        connections.forEach(connection =>
          app.channel(`rooms/${participation.roomId}`).join(connection)
        );

        return context;
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
