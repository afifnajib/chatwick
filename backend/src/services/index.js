import messages from './messages/messages.service';
import participants from './participants/participants.service';
import rooms from './rooms/rooms.service';
import users from './users/users.service';

export default function (app) {
  app.configure(messages);
  app.configure(participants);
  app.configure(rooms);
  app.configure(users);
}
