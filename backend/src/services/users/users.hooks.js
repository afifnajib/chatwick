import {hooks} from '@feathersjs/authentication';
import {compileSchema} from 'feathers-hook-validation-jsonschema';

const {authenticate} = hooks;

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

const validate = compileSchema({
  title: 'User Signup Schema',
  type: 'object',
  properties: {
    email: {
      type: 'string',
      format: 'email',
      maxLength: 255
    },
    password: {
      type: 'string',
      minLength: 6,
      maxLength: 255
    },
    username: {
      type: 'string',
      minLength: 2,
      maxLength: 255
    }
  },
  required: ['email', 'password', 'username']
});

export default {
  before: {
    all: [],
    find: [authenticate('jwt')],
    get: [authenticate('jwt')],
    create: [validate, hashPassword()],
    update: [validate, hashPassword(), authenticate('jwt')],
    patch: [validate, hashPassword(), authenticate('jwt')],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
