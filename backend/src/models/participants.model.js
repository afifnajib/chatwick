// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

const {DataTypes} = Sequelize;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const participants = sequelizeClient.define('participants', {
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false
    },
    roomId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  participants.removeAttribute('id');

  // eslint-disable-next-line no-unused-vars
  participants.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    participants.belongsTo(models.users, {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
    participants.belongsTo(models.rooms, {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE'
    });
  };

  return participants;
};
