import path from 'path';
import compress from 'compression';
import configuration from '@feathersjs/configuration';
import cors from 'cors';
import express from '@feathersjs/express';
import favicon from 'serve-favicon';
import feathers from '@feathersjs/feathers';
import helmet from 'helmet';
import logger from 'winston';
import socketio from '@feathersjs/socketio';

import appHooks from './app.hooks';
import authentication from './authentication';
import channels from './channels';
import middleware from './middleware';
import sequelize from './sequelize';
import services from './services';

const app = express(feathers());

// Load app configuration
app.configure(configuration());
// Enable CORS, security, compression, favicon and body parsing
app.use(cors());
app.use(helmet());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio());

app.configure(sequelize);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({logger}));

app.hooks(appHooks);

export default app;
