'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'users',
      'username',
      {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      'users',
      'username',
      {
        allowNull: true,
        type: Sequelize.STRING,
        unique: false
      }
    );
  }
};
