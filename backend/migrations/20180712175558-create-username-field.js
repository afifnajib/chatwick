'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'users',
      'username',
      {
        type: Sequelize.STRING
      }
    );
  },

  down: queryInterface => {
    return queryInterface.removeColumn(
      'users',
      'username'
    );
  }
};
