# Vue Avenue: Video #1 - Intro

- Afif Sohaili.
- Engineer @ ServiceRocket.
  - Using a wide-range of tools at work.
  - React, Vue, Serverless, Javascript, Typescript, Scala.
  - Love Vue and use it in my pet projects.
- Tutorial from 2 years ago.
  - Nuxt was still 0.x.
  - Feathers was v2.
- Recreating the tutorial.
  - Goes deeper:
    - Chat app with rooms.
    - Managing migrations.
    - Building UIs and lil bit of animation (using <transition> component).
  - Series of short videos.
    - Less editing to do.
    - You can navigate by topic.
- Series Objective:
  - Teach you how to build a production-grade app from zero to deployment.
    - Not a chat app tutorial.
    - One of the comments I received:
      - Don't need SSR for a chat app.
      - True, but I am not teaching you guys how to build just a chat app.
    - A huge majority of the apps you're using is just CRUD apps.
    - Don't need realtime connectivity for most cases, but if you do, this got it covered.
  - Will also be covering deployments and CI/CD.
- Tools:
  - Node v8+.
  - npm or yarn.
  - Postgresql for database.
  - Nuxt for frontend.
    - Easy to do SSR.
    - Zero configuration to start with.
  - Feathers for backend.
    - Easy to start with.
    - Have good generators.
    - Gives realtime connectivity out of the box with Socket.io.
  - Testing frameworks:
    - Ava for unit tests and snapshot tests.
    - Cypress for e2e.
    - Docker & docker-compose.
  - Deployments:
    - pm2 & linode.
    - Docker & AWS ECS.
- Hope for the series:
  - It'll be useful for others.
  - If you're building an app from scratch.
- Hope for the community:
  - Leave constructive criticisms/better suggestions.
    - Learnt a lot from comments on the previous video.
- Continue to next video:

# Prequel: Promises.

- Dealing with asynchronous code.
  - Callback:
    - Demo callback hell.
  - Promise:
    - Demo promise as an alternative to callbacks.
  - async/await:
    - Convert promise code to async/await.
    - When is it useful.
    - Demonstrate Promise.all.

# Lesson #1:

## Objective:
- Initialize frontend project.

## Steps:
- Generate project with vue-cli.
- Should have basic Nuxt app working.
- Change site title.
- Change basic styles - background color.
- Delete default components.
- Install xo for linting.

# Lesson #2:

## Objective:
- Add basic stylings.
- Introduce basic component and generic page layouts.

## Steps:
- Add & configure nuxt-buefy.
- Delete default components.
- Introduce basic header with branding.

# Lesson #3:

## Objective:
- Add LoginForm view component.

## Steps:
- Create LoginForm view component.
- Add unit tests & snapshot tests (if applicable).

# Lesson #4:

## Objective:
- Initialize backend.

## Steps:
- Install feathers-cli, generate app.
- Add xo for linting.
- Add backpack-core for ES6.

# Lesson #5:

## Objective:
- Generate authentication service on backend.
- Start writing migration scripts.

## Steps:
- Generate authentication with feathers-cli.
- Disable sequelize.sync, create migration script for users table.
- Add new user via Postman.

# Lesson #6:

## Objective:
- Wire up authentication logic on frontend.

## Steps:
- Fix login form not wrapped in <form> tag.
- Initialise store with auth key (auth.js).
- On login, redirect to next page or home page.

# Lesson #7:

## Objective:
- Writing e2e test for login.
- Start seeding data.

## Steps:
- Fix login form not wrapped in <form> tag.
- Initialise store with auth key (auth.js).
- On login, redirect to next page or home page.

# Lesson #8:

## Objective:
- Adding an attribute to an existing model.
- Prequel to sign up form (needs username on User model).

## Steps:
- Add username field to users (w/o unique and notNull).
- Populate username field on existing user records.
- Add another migration script to add unique and notNull constraints.

# Lesson #9:

## Objective:
- Add sign up functionality.

## Steps:
- Add sign up component.
- Add sign up e2e spec.

# Lesson #10:

## Objective:
- Tools investment: Set up docker-compose to run e2e tests.

# Lesson #11:

## Objective:
- Add /rooms service.
- Add page /rooms to list rooms.

## Steps:
- Generate rooms service on backend.
- Create a few rooms via Postman.
- Add links to /rooms on header.
- Add basic /rooms page to list rooms.
- Add e2e spec for rooms list.

# Lesson #11:

## Objective:
- Consolidated vuex service for feathers client.
- Introduce feathers rest-client.

# Lesson #12:

Objective:
- Create room page.

## Steps:
- Add "Create New Room" page.
- Add basic rooms/:_id page with just room name header.
- Wire up logic for creating new room.
- Redirect to rooms/:_id page after creating a room.
- E2E specs.

# Lesson #13:

## Objective:
- Enable participation.

## Steps:
- Add /participants service with migration script.
- Add leave/join button on the room page.
- Join socket.io channel once participation record is created.

# Lesson #14:

## Objective:
- To be able to send new chat messages from the room page.

## Steps:
- Create chat composer component.
- Detect if user is a participant - if it is, disable the composer.

# Lesson #15:

## Objective:
- Manage state of messages loaded when viewing a room page.

## Steps:
- Create store/chat.js vuex module.
- Load all messages upon viewing a room page and joined the room.

# Lesson #16:

## Objective:
- Handle leaving room.

## Steps:
- Clear messages from vuex.
- Don't listen to created events anymore.

# Lesson #17:

## Objective:
- Improve chat messages appearance.

## Steps:
- Create Chatbubble component.
- Differentiate between message creator's message and others'.
